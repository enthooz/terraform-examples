## Usage
Ensure you have `aws-cli` setup properly with credentials in place.
``` bash
cd src

cp terraform.tfvars-example terraform.tfvars

# modify values in terraform.tfvars as needed

terraform init
terraform apply
```
