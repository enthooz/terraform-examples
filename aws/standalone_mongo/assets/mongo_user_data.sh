#!/bin/bash

# update package list
apt update

# install MongoDB
apt install -y mongodb

# bind mongod to external IP
sed -i "s/bind_ip = 127.0.0.1/bind_ip = 127.0.0.1,`hostname -I`/g" /etc/mongodb.conf

# restart mongod
systemctl restart mongodb
