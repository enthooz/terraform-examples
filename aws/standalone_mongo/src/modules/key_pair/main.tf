resource "aws_key_pair" "main" {
  key_name   = var.name_tag
  public_key = file(var.ssh_public_key_path)
}
