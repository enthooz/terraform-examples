variable "key_pair_name" {}
variable "security_group_id" {}
variable "subnet_id" {}
variable "ubuntu_version" {}
variable "instance_size" {}
variable "name_tag" {}
