resource "aws_security_group" "main" {
  vpc_id      = var.vpc_id
  name        = var.name_tag
  description = "Enables SSH on specific IPs/CIDRs"
}

resource "aws_security_group_rule" "ssh" {
  security_group_id = aws_security_group.main.id
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = [var.remote_cidr]
}

resource "aws_security_group_rule" "mongo" {
  security_group_id = aws_security_group.main.id
  type              = "ingress"
  from_port         = 27017
  to_port           = 27017
  protocol          = "tcp"
  cidr_blocks       = [var.remote_cidr]
}

resource "aws_security_group_rule" "http_out" {
  security_group_id = aws_security_group.main.id
  type              = "egress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  ipv6_cidr_blocks  = ["::/0"]
}

resource "aws_security_group_rule" "https_out" {
  security_group_id = aws_security_group.main.id
  type              = "egress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  ipv6_cidr_blocks  = ["::/0"]
}
