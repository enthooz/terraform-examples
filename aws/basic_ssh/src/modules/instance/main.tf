# Searches for the latest Ubuntu AMI using provided version in form
# "CODENAME-XX.YY", e.g. "bionic-18.04".
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-${var.ubuntu_version}-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "mongodb" {
  key_name               = var.key_pair_name
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = var.instance_size
  vpc_security_group_ids = [var.security_group_id]
  subnet_id              = var.subnet_id

  tags = {
    Name = var.name_tag
  }
}

resource "aws_eip" "ip" {
  vpc      = true
  instance = aws_instance.mongodb.id

  tags = {
    Name = var.name_tag
  }

  provisioner "local-exec" {
    command = "echo Instance IP: ${self.public_ip}"
  }
}
