resource "aws_security_group" "main" {
  vpc_id      = var.vpc_id
  name        = var.name_tag
  description = "Enables SSH on specific IPs/CIDRs"
}

resource "aws_security_group_rule" "ssh" {
  security_group_id = aws_security_group.main.id
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = [var.remote_cidr]
}
