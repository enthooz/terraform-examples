resource "aws_subnet" "main" {
  vpc_id     = var.vpc_id
  cidr_block = var.subnet_cidr

  tags = {
    Name = var.name_tag
  }
}

# NOTE: one could also use `aws_default_route_table` to modify the VPC's
# default route table instead of adding a new `aws_route_table`
resource "aws_route_table" "main" {
  vpc_id     = var.vpc_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = var.gateway_id
  }

  tags = {
    Name = var.name_tag
  }
}

# NOTE: Should we also add a `gateway_id` association?  Would require a
# separate `aws_route_table_association` block, and show up as an "Edge
# Association" in AWS Console.
resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.main.id
  route_table_id = aws_route_table.main.id
}
