resource "aws_vpc" "main" {
  cidr_block = var.vpc_cidr

  tags = {
    Name = var.name_tag
  }
}

# # EXAMPLE: how to add additional CIDR blocks to VPC
# resource "aws_vpc_ipv4_cidr_block_association" "secondary_cidr" {
#   vpc_id     = "${aws_vpc.main.id}"
#   cidr_block = "172.2.0.0/16"
# }

resource "aws_internet_gateway" "gateway" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = var.name_tag
  }
}
