# NOTE: one could also use `aws_default_network_acl` to modify the VPC's
# default network ACL instead of adding a new `aws_network_acl`
resource "aws_network_acl" "main" {
  vpc_id     = var.vpc_id
  subnet_ids = [var.subnet_id]

  tags = {
    Name = var.name_tag
  }

  # allow ingress port 22 (SSH)
  ingress {
    rule_no    = 100
    protocol   = "tcp"
    action     = "allow"
    cidr_block = var.remote_cidr
    from_port  = 22
    to_port    = 22
  }

  # allow egress on ephemeral ports
  egress {
    rule_no    = 100
    protocol   = "tcp"
    action     = "allow"
    cidr_block = var.remote_cidr
    from_port  = 1024
    to_port    = 65535
  }
}
