module "vpc" {
  source   = "./modules/vpc"
  vpc_cidr = var.vpc_cidr
  name_tag = var.name_tag
}

module "security_group" {
  source      = "./modules/security_group"
  vpc_id      = module.vpc.id
  remote_cidr = var.remote_cidr
  name_tag    = var.name_tag
}

module "subnet" {
  source      = "./modules/subnet"
  vpc_id      = module.vpc.id
  gateway_id  = module.vpc.internet_gateway_id
  subnet_cidr = var.subnet_cidr
  name_tag    = var.name_tag
}

module "network_acl" {
  source      = "./modules/network_acl"
  vpc_id      = module.vpc.id
  subnet_id   = module.subnet.id
  remote_cidr = var.remote_cidr
  name_tag    = var.name_tag
}

module "key_pair" {
  source              = "./modules/key_pair"
  ssh_public_key_path = var.ssh_public_key_path
  name_tag            = var.name_tag
}

module "instance" {
  source            = "./modules/instance"
  key_pair_name     = module.key_pair.name
  security_group_id = module.security_group.id
  subnet_id         = module.subnet.id
  ubuntu_version    = var.ubuntu_version
  instance_size     = var.instance_size
  name_tag          = var.name_tag
}
